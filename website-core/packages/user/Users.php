<?php


namespace Core\Packages\user;

use Core\Packages\file_manager\src\models\UploadFile;
use Core\System\Exceptions\CoreException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Core\System\Http\Traits\HelperTrait;
class Users extends Authenticatable implements JWTSubject
{
    use HelperTrait;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    private static $_instance = null;
    public static function _()
    {
        if (self::$_instance == null) {
            self::$_instance = new Users();
        }
        return self::$_instance;
    }
    protected function createNewToken($token)
    {
        return $this->modelResponse(['data' => [
            'access_token' => $token,
//            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]]);
    }
    protected function login_response()
    {
        $user = Auth('web')->user();
        return $this->modelResponse(['data' => [
            'user' => $user
        ]]);
    }
    public function register($payload)
    {
        try {
            $payload['password'] =  bcrypt($payload['password']);
            $result = Users::create($payload);
            Auth::shouldUse('web');
            Auth::login($result);
            return $this->login_response();
        } catch (\Exception $e) {
            return $this->errorHandler($e);
        }
    }
    public function login($payload)
    {

        if ( auth('web')->attempt($payload)) {
            if(auth('web')->check()){
                Auth::shouldUse('web');
                return $this->login_response();
            }

        }else{
            throw new CoreException('auth.failed',401);
        }

    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];

    }

    //relations

    public function uploadFiles()
    {
        return $this->morphMany(UploadFile::class, 'fileable');
    }


    // methods
    public function getUserFiles($user_id){
        return $this->find($user_id)->uploadFiles;
    }
}
