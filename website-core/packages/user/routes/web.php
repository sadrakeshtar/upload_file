<?php

use Core\Packages\file_manager\src\controllers\FileManagerController;
use Core\Packages\user\src\controllers\UserPackageController;
use Illuminate\Support\Facades\Route;

$prefix = '/users';


Route::group(['middleware' => ['web']], function () use ($prefix) {
    Route::post('/login', [UserPackageController::class, 'login'])->name('users.login');
    Route::get('/login', [UserPackageController::class, 'login_index'])->name('login');
    Route::get('/register', [UserPackageController::class, 'register_index'])->name('users.register');
    Route::post('/register', [UserPackageController::class, 'register'])->name('register');
    Route::get('/', [FileManagerController::class, 'index'])->name('index');
});
