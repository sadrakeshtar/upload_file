<?php

namespace core\Packages\user\src\controllers;

use Core\Packages\user\src\request\UpdateRequest;
use Core\System\Http\Controllers\CoreController;
use Core\Packages\user\src\request\RegisterRequest;
use Core\Packages\user\src\request\LoginRequest;
use Core\Packages\user\Users;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class UserPackageController
 *
 * @package Core\Packages\user\src\controllers
 */
class UserPackageController extends CoreController
{


    private $_register = [
        'name',
        'email',
        'password',
    ];

    private $_login = [
        'email',
        'password'
    ];

    public function login_index()
    {
        if (Auth::check() ) {
            return redirect()->route('file_manager.index');

        }
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/auth/login', [
            'pageConfigs' => $pageConfigs
        ]);
    }
    public function register_index(){
        if (Auth::check() ) {
            return redirect()->route('file_manager.index');

        }

        $pageConfigs = ['blankPage' => true];

        return view('/auth/register', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    public function register(RegisterRequest $request)
    {
        $payload = $request->only($this->_register);
        $result = Users::_()->register($payload);
        return $this->responseHandler($result);
    }

    public function login(LoginRequest $request)
    {

        $payload = $request->only($this->_login);
        $result = Users::_()->login($payload);

        return $this->responseHandler($result);
    }

    public function logout()
    {
        $result = Users::_()->logout();
        return $this->responseHandler($result);
    }

    public function profile()
    {

        $result = Users::_()->profile();
        return $this->responseHandler($result);

    }

}
