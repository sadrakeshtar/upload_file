<?php

namespace Core\Packages\file_manager;

use Core\System\Exceptions\c2c\C2CException;
use Core\System\Providers\PackableServiceProvider;
use Facade\Ignition\ErrorPage\Renderer;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Mockery\Exception;

class FileManagerServiceProvider extends PackableServiceProvider
{
    /**
     * @var string
     */
    protected $DIR = __DIR__;


    /**
     * @var string
     */
    protected $NAMESPACE = 'Core\Packages\file_manager\src\controllers';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadMigrationsFrom($this->DIR.'/database'.DIRECTORY_SEPARATOR.'migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
