<?php
use Core\Packages\c2c\src\controllers\ApiController;
use Core\Packages\c2c\src\controllers\PaymentController;
use Core\Packages\c2c\src\controllers\GatewayController;
use Illuminate\Support\Facades\Route;
$prefix  = config('api.prefix') .'/file_manager';
