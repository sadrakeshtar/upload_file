<?php

use Core\Packages\file_manager\src\controllers\FileManagerController;
use Illuminate\Support\Facades\Route;

$prefix = '/file_manager';

Route::group(['prefix' => $prefix, 'middleware' => [ 'web']], function () {

    Route::group(['middleware' => ['package_auth:web']], function () {
        Route::post('upload_file', [FileManagerController::class, 'upload_file'])->name('file_manager.upload_file');
        Route::get('/', [FileManagerController::class, 'index'])->name('file_manager.index');
        Route::post('/delete_file', [FileManagerController::class, 'delete_file'])->name('file_manager.delete_file');
    });
    Route::get('/download_file/{user}/{file_name}', [FileManagerController::class, 'download_file'])->name('file_manager.download_file');
});
