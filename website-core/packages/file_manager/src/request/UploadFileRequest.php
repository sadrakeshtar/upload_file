<?php

namespace Core\Packages\file_manager\src\request;

use Core\Packages\file_manager\src\rules\IsValidFormat;
use Core\System\Http\Requests\FormRequestCustomize ;

class UploadFileRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required','max:10000',new IsValidFormat()],
        ];
    }
}
