<?php

namespace Core\Packages\file_manager\src\request;

use Core\Packages\file_manager\src\rules\IsValidFormat;
use Core\System\Http\Requests\FormRequestCustomize ;

class DeleteFileRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files' => ['required','array',"exists:upload_files,id"],
        ];
    }
}
