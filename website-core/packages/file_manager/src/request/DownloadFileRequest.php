<?php

namespace Core\Packages\file_manager\src\request;

use Core\Packages\file_manager\src\rules\IsValidFormat;
use Core\System\Http\Requests\FormRequestCustomize ;

class DownloadFileRequest extends FormRequestCustomize
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

    }
}
