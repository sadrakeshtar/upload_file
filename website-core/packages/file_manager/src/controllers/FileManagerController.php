<?php

namespace core\Packages\file_manager\src\controllers;

use Core\Packages\file_manager\src\models\UploadFile;
use Core\Packages\file_manager\src\request\DeleteFileRequest;
use Core\Packages\file_manager\src\request\DownloadFileRequest;
use Core\Packages\file_manager\src\request\UploadFileRequest;
use Core\Packages\user\Users;
use Core\System\Http\Controllers\CoreController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

/**
 * Class UserPackageController
 *
 * @package Core\Packages\user\src\controllers
 */
class FileManagerController extends CoreController
{

    public function index()
    {
        $user = auth('web')->user();
        if (!$user) {
            return redirect()->route('login');
        }
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'file-manager-application',
        ];
        $disk_free_space = disk_free_space('/');
        $disk_total_space = disk_total_space('/');

        $diskSize = [
            "disk_total_space" => $disk_total_space,
            "disk_free_space" => $disk_free_space,
            "disk_used_space" => $disk_total_space - $disk_free_space,
        ];


        $files = Users::_()->getUserFiles($user->id);
        return view('/content/apps/fileManager/app-file-manager', ['pageConfigs' => $pageConfigs, 'diskSize' => $diskSize, 'files' => $files, "user"=>$user]);
    }


    //store
    public function upload_file(UploadFileRequest $request)
    {

        $file = $request->file('file'); //get file from request
        $user = auth('web')->user();
        if ($user) {
            if ($file) {
                $originalFile = $file->getClientOriginalName();
                $store = Storage::disk('public')->put($user->name, $file);
                $uploadFile = new UploadFile(['name' => $originalFile, 'file_name' => $store]);
                $user_record = Users::find($user->id);
                $result = $user_record->uploadFiles()->save($uploadFile);

                return $this->responseHandler($result, 'create');
            }
        }


    }

    public function delete_file(DeleteFileRequest $request)
    {

        $result = UploadFile::destroy($request['files']);
        return $this->responseHandler($result, 'delete');
    }

    public function download_file(Request $request, $user, $file_name)
    {
        if ($user and $file_name) {
            $file_path = $user . "/" . $file_name;
            $file = UploadFile::_()->findFileByName($file_path);
            if ($file){
                $download_count = $file->download_count + 1 ;
                UploadFile::_()->updateRow(['download_count'=> $download_count],$file->id);
                if ( Storage::disk('public')->exists($file_path)){
                    return Storage::disk('public')->download($file_path);
                }
                abort(404);
            }else{
                abort(404);
            }
        }

    }
}
