<?php

namespace Core\Packages\file_manager\src\models;

use Core\System\Http\Traits\HelperTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UploadFile extends Model
{
    use HasFactory;
    use HelperTrait;
    protected $fillable = [
        'name',
        'file_name',
        'download_count',
    ];
    protected $appends = [
        'url'
    ];
    protected $hidden = [
        'fileable_type',
    ];
    private static $_instance = null;

    public static function _()
    {
        if (self::$_instance == null) {
            self::$_instance = new UploadFile();
        }
        return self::$_instance;
    }

    public function fileable()
    {
        return $this->morphTo();
    }
    public function getUrlAttribute($value)
    {
        return Storage::url($this->file_name);
    }
    public function findFileByName($file_name){
       return $this->where('file_name',$file_name)->firstOrFail();
    }

}
