<?php

namespace Core\Packages\file_manager\src\rules;

use Illuminate\Contracts\Validation\Rule;

class IsValidFormat implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {   //exe file mimetype
        $exeMimType = ['pplication/octet-stream','application/x-dosexec', 'application/x-msdownload', 'application/exe', 'application/x-exe', 'application/dos-exe, vms/exe', 'application/x-winexe', 'application/msdos-windows', 'application/x-msdos-program'];

        $mimeTypeFile = $value->getMimeType();
        if (in_array($mimeTypeFile,$exeMimType) or $mimeTypeFile == "image/bmp" or $mimeTypeFile == "text/x-php"  ){
            return false;
        }
        return  true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'File Not mime type in .php , .exe , .bmp';
    }
}
