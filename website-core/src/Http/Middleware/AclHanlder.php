<?php

namespace Core\System\Http\Middleware;

use Closure;
use DB;

class AclHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userInfo = auth('api')->user()->toArray();
        $roles = DB::table('user_roles')->where('user_id', $userInfo['id'])->pluck('role_id')->toArray();
        if (count($roles)) {
            $routeName = $request->route()->getName();
            $translate = config('roles.routes.access');
            $translateName = !empty($translate[$routeName]) ? $translate[$routeName] : $routeName;
            $permissions = DB::table('role_permissions')->whereIn('role_id', $roles)->pluck('name')->toArray();
            if (count($permissions)) {
                $hasAccess = false;
                if (in_array($routeName, $permissions) == true) {
                    $hasAccess = true;
                }

                if ($hasAccess == false) {
                    $message = " شما به " . $translateName . " دسترسی ندارید";
                    return response(['hasError' => true, 'message' => [$message]]);
                }
            }
        }
        return $next($request);
    }
}
