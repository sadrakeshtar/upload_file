<?php

namespace  Core\System\Http\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    public function responseHandler($data,$type = null)
    {
        $messages = '';
        if (!empty($data->hasError) && $data->hasError == true) {
            $messages = $data->message;
        } else {
            if (!empty($data->error)) {
                $messages = $data->error_description;
            }else{

                if ((isset($data->result)  or isset($data) )and isset($type) and $type == "create"){
                    return [
                        'status'  => true,
                        'message' =>"operation done successfully",
                        'data'      => $data->result ?? $data,
                    ];
                }elseif((isset($data->result)  or isset($data) ) and isset($type) and $type == "delete"){
                    return [
                        'status'  => true,
                        'message' =>"item deleted successfully",
                    ];
                }elseif((isset($data->result)  or isset($data) ) and isset($type) and $type == "update"){
                    return [
                        'status'  => true,
                        'message' =>"item updated successfully",
                    ];
                }

            }
        }

        if (isset($data->message) and empty($messages) ){
            $messages = $data->message;
        }
        if (!empty($messages) && !is_array($messages)) {
            $messages = [
                $messages
            ];
        }
        $responseRecord = !empty($data->result) ? $data->result : [];
        return response()->json($responseRecord, 200, ['Content-Type' => 'application/json;charset=utf8'],
        JSON_UNESCAPED_UNICODE);
    }


}
