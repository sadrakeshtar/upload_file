<?php

namespace Core\System\Http\Traits;

use Core\System\Exceptions\CoreException;

trait HelperTrait
{
    public $_COLOR = [
        "primary",
        "warning",
        "success",
        "danger",
        "info",
    ];

    public function show($id)
    {
        $result = Routes::find($id);
        if (!isset($result)) {
            throw new CoreException(' شناسه ' . $id . ' یافت نشد');
        }
        return $this->modelResponse(['data' => $result]);
    }
    static function is_dir_empty($dir) {
        if (!is_readable($dir)) return NULL;
        return (count(scandir($dir)) == 2);
    }
    public static function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    public function insertRow($payload)
    {

        try {
              $response = $this->create($payload);
            return $response;
        } catch (\Exception $e) {
            return $this->errorHandler($e->getMessage());
        }
    }

    public function destroyRow($id)
    {

        try {
            if (is_array($id)) {
                $record = $this->whereIn('id', $id)->get();
            }

            if (count($record) < 1) {
                throw new CoreException(' شناسه های ' . implode(",", $id) . ' یافت نشد');
            }

            $response = $record->each->delete();
            return $this->modelResponse(['data' => $response]);
        } catch (\Exception $e) {
            return $this->errorHandler($e->getMessage());
        }
    }

    public function updateRow($payload, $id)
    {
        $record = $this->find((int)$id);
        if (!isset($record)) {
            throw new CoreException(' شناسه ' . $id . ' یافت نشد');
        }
        try {
            $record->update($payload);
            return $this->modelResponse(['data' => $record]);
        } catch (\Exception $e) {
            return $this->errorHandler($e);
        }
    }

    public function modelResponse($data, $type = false,$error = false, $message = '')
    {
        if (!empty($error) && $error == true) {
            $messages = $message;
        }
        if (!empty($messages) && !is_array($messages)) {
            $messages = [
                $messages
            ];
        }
        return (object)[
            'message' => !empty($messages) ? $messages : [],
            'type' => $type,
            'count' => !empty($data['count']) ? $data['count'] : 0,
            'result' => (!empty($data['data'])) ? $data['data'] : []
        ];
    }

    public function errorHandler($e)
    {
        if (isset($e->errorInfo)) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                $validations = \Lang::get('validation');
                preg_match("#for\skey\s'(\w+)'#", $e->getMessage(), $fields);
                if (!empty($fields)) {
                    $column = str_replace('_unique', '', $fields[1]);
                    if (isset($column, $validations['attributes'][$column])) {
                        throw new CoreException("فیلد {$validations['attributes'][$column]} تکراری می باشد");
                    } else {
                        throw new CoreException("فیلد مورد نظر تکراری می باشد");

                    }
                }
            }
        }
//        if(!empty(@$e->getMessage())){
//            throw new CoreException($e->getMessage());
//        }
        throw new CoreException($e);
    }

}
