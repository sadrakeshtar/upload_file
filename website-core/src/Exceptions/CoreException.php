<?php


namespace Core\System\Exceptions;


use Exception;
use Illuminate\Support\Facades\Lang;

class CoreException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */

    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function render($request)
    {

        return $this->sendJsonError(Lang::get($this->message) ?? $this->message,$this->code ? $this->code : 400  );
    }

    public function sendJsonError($message,$header)
    {

        return response()->json([
            'has_error' => false,
            'message' =>$message
        ], $header);
    }

}
