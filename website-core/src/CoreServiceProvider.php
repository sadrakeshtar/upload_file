<?php

namespace Core\System;

use Core\System\Http\Middleware\AclHandler;
use Core\System\Http\Middleware\JwtHandler;
use Core\System\Http\Middleware\PackageAuth;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Core\System\Providers\PackageServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {

        $this->registerClassServices();
        $this->configLocalType();

        if (class_exists(JwtHandler::class)) {

            $router->aliasMiddleware('jwt_handler', JwtHandler::class);
        }
        if (class_exists(AclHandler::class)) {
            $router->aliasMiddleware('acl_handler', AclHandler::class);
        }
        if (class_exists(PackageAuth::class)) {
            $router->aliasMiddleware('package_auth', PackageAuth::class);
        }

    }

    public function registerClassServices()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $this->app->register(PackageServiceProvider::class);

    }

    public function configLocalType()
    {
        $local = config('core.local');
        if ($local == 'fa') {
            App::setLocale(config('core.local'));
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configs = [
            'core' => 'core.php',
            'api' => 'api.php',
        ];
        foreach ($configs as $key => $config) {
            $this->mergeConfigFrom(__DIR__ . '/../config/' . $config, $key);
        }
    }
}
