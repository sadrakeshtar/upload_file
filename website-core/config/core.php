<?php
return [
    'controllers' => [
        'namespace' => 'Core\\System\\Http\\Controllers'
    ],
    'packages_controllers' => [
        'namespace' => 'Core\\Packages'
    ],
    'local' => 'fa',
    'packages' => [
        \Core\Packages\user\UserServiceProvider::class,
        \Core\Packages\file_manager\FileManagerServiceProvider::class,
    ],
    'prefix' => 'core'
];
